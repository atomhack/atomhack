import cv2 as cv
import numpy as np
import math
from matplotlib import pyplot as plt
img = cv.imread('1.bmp', cv.IMREAD_GRAYSCALE)
assert img is not None, "file could not be read, check with os.path.exists()"
#img = cv.bitwise_not(img)
brightness = cv.mean(img)[0]
# определение пороговых значений
t1 = 1 * brightness
t2 = 1.2 * np.max(img)

edges = cv.Canny(img,t1,t2)
contours, hierarchy = cv.findContours(edges, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
cImg = img.copy()
gImg = img.copy()
centers = []
#cv.drawContours(img, contours, -1, (255,255,0), 3)
# loop over the contours
MAX_W = 0
for c in contours:
  c = cv.convexHull(c)
	# compute the center of the contour
  M = cv.moments(c)
  M['m00']+=0.00000000001
  cX = int(M['m10']/M['m00'])
  cY = int(M['m01']/M['m00'])
  cW = cv.contourArea(c)+0.00000000001
	# draw the contour and center of the shape on the image
  cv.drawContours(cImg, [c], -1, (0, 255, 0), 2)
  cv.circle(cImg, (cX, cY), 7, (255, 255, 255), -1)
  cv.putText(cImg, "center", (cX - 20, cY - 20),
    cv.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
  centers.append((cX,cY,cW))

def clusterize(S,distance):
    coords=set(S)
    C=[]
    points = []
    while len(coords):
        locus=coords.pop()
        cluster = [x for x in coords if math.dist((locus[0],locus[1]),(x[0],x[1])) <= distance]
        C.append(cluster+[locus])
        for x in cluster:
            coords.remove(x)
    for cluster in C:
      pX = 0
      pY = 0
      pW = 0
      for point in cluster:
        pW += point[2]
      for point in cluster:
        pX += point[0]*(point[2]/pW)
        pY += point[1]*(point[2]/pW)
        
      points.append([int(np.ceil(pX)),int(np.ceil(pY)),pW])

    return points
clusterPoints = clusterize(centers,64)
for point in clusterPoints:
  cv.circle(gImg, (point[0], point[1]), int(0.1*point[2]), (255, 255, 255), -1)
  cv.putText(gImg, "center", (point[0] - 20, point[1] - 20),
    cv.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

plt.figure(figsize=(18,12))
plt.subplot(221),plt.imshow(img, cmap='gray')
plt.title('Оригинальное изображение'), plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(edges, cmap='gray')
plt.title('Контуры потенциальных дефектов'), plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(cImg, cmap='gray')
plt.title('Центры масс контуров'), plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(gImg, cmap='gray')
plt.title('Кластеризация центров'), plt.xticks([]), plt.yticks([])
plt.show()