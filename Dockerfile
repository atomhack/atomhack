FROM python:3.10
WORKDIR /atomhack
COPY server.py .
COPY requirements.txt .
RUN pip install -r requirements.txt
CMD python server.py
EXPOSE 5000

