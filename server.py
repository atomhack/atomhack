import tensorflow as tf
from keras.preprocessing.image import img_to_array
from PIL import Image, ImageDraw
import numpy as np
import flask
import io
import uuid
import os
import cv2 as cv2


w,h = (512,768)

def load_model():

    with open('model/config.json') as json:
        model = tf.keras.models.model_from_json(json.read())

    model.load_weights('model/model_weights.h5', by_name=True)

    return model

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
model = load_model()
ALLOWED_EXTENSIONS = {'bmp'}
CLASSES_LIST = ['inclusion', 'waist folding', 'rolled_pit', 'oil_spot', 'crescent_gap', 'crease', 'water_spot', 'punching_hole', 'silk_spot', 'welding_line']
IMG_FOLDER = 'images/'

def prepare_image(image, target):
	# if the image mode is not L, convert it
    if image.mode != "L":
        image = image.convert("L")

	# resize the input image and preprocess it
    image = image.resize(target)
    image = img_to_array(image)



    image = np.expand_dims(image, axis=0)
    
    # return the processed image
    return image

@app.route("/api/predict", methods=["POST"])
def predict():
	# initialize the data dictionary that will be returned from the
	# view
    data = {"success": False}

    # ensure an image was properly uploaded to our endpoint
    if flask.request.method == "POST":
        if flask.request.files.get("image"):
            # read the image in PIL format
            image = flask.request.files["image"].read()
            image = Image.open(io.BytesIO(image))
            # preprocess the image and prepare it for classification
            image = prepare_image(image, target=(w, h))
            result = model.predict(image)
            data["predictions"] = []
            defect_class = np.argmax(result[0][0])
            label = CLASSES_LIST[defect_class]
            prob = result[0][0][defect_class]
            r = {"label": label, "probability": prob, "position_x": result[1][0][0], "position_y":result[1][0][1]}
            data["predictions"].append(r)
			# indicate that the request was a success
            data["success"] = True
    # return the data dictionary as a JSON response
    return flask.jsonify(data)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
@app.route('/', methods=['GET', 'POST'])
def index():
    if flask.request.method == 'POST':
        # check if the post request has the file part
        if 'image' not in flask.request.files:
            flask.flash('No file part')
            return flask.redirect(flask.request.url)
        file = flask.request.files['image']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flask.flash('No selected file')
            return flask.redirect(flask.request.url)
        if file and allowed_file(file.filename):
            img = Image.open(io.BytesIO(file.read()))
            result = model.predict(prepare_image(img, target=(w, h)))
            #paint defect border and class
            defect_class = np.argmax(result[0][0])
            defect_class = CLASSES_LIST[defect_class] + " " + str(result[0][0][defect_class])
            center_x = result[1][0][0]*w
            center_y = result[1][0][1]*h
            size = 128 // 2
            x1 = center_x - size 
            y1 = center_y - size 
            x2 = center_x + size 
            y2 = center_y + size 
            res_img = img.convert("RGB")
            res_img = res_img.resize((w,h))
            draw = ImageDraw.Draw(res_img)
            draw.rectangle((x1, y1, x2, y2), outline=(255, 0, 0))
            draw.text((center_x,center_y+size+10),defect_class,fill=(255,0,0))
            #save image
            res_img_url = IMG_FOLDER+str(uuid.uuid4())+'.bmp'
            res_img.save(res_img_url, format="BMP")
            return f'''
                <!doctype html>
                <title>Upload new File</title>
                <h1>Upload new File</h1>
                <form method=post enctype=multipart/form-data>
                <input type=file name=image>
                <input type=submit value=Upload>
                </form>
                <img src='{res_img_url}'/>
                '''

    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=image>
      <input type=submit value=Upload>
    </form>
    '''
@app.route('/images/<image_name>')
def get_image(image_name):
    if os.path.exists(os.path.join('images', image_name)):
        return flask.send_file(os.path.join('images', image_name), mimetype='image/bmp')
    else:
        return 'Image not found', 404
    
if __name__ == "__main__":
    print(("* Loading model and Flask starting server..."
        "please wait until server has fully started"))
    app.secret_key = '///blank///'
    app.config['SESSION_TYPE'] = 'memcache'
    app.run()